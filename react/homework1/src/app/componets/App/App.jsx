import React from 'react';
import styles from './App.css';
import reset from './../../../shared/styles/css/reset.css'
import Header from '../../../client/example/components/Header/components/Header/Header';
import Main from '../../../client/example/components/Main/components/Main/Main';
import './../../../../node_modules/font-awesome/css/font-awesome.min.css';
import CategoryList from './../../../client/example/components/Main/components/CategoryList/CategoryList';
import Subscribe from './../../../client/example/components/Subscribe/Subscribe'
import Footer from './../../../client/example/components/Footer/Footer'
const categories = ['Trending','Top Rated', 'New Arrivals', 'Trailers','Coming Soon','Genre']
export const App = () => {
    return (
        <div className ='head-page'>
            <div className="main-app">
                <Header/>
                <CategoryList text={categories}/>
                <Main count='12'/>
                <Subscribe/>
                <Main count='4'/>
                <div className ='loader'>
                    <span className='loader-icon'><i className="fa fa-ellipsis-h"></i></span>
                    <p className='loader-text'>Loading</p>
                </div>
            </div>
            <Footer/>
        </div>
        
    );
};
