import React from 'react';
import style from './MovieList.css';
import MovieItem from './../../../../../../shared/components/MovieItem/MovieItem'
const MovieList = ({list,count}) =>{
    const arr = list.slice(0,count)
    const movies = arr.map(e=><MovieItem params={e}/>)
    return(
        <div className='movie-list'>
            {movies}
        </div>
    )
}

export default MovieList