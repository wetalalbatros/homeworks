import React from 'react';
import styles from './Main.css';
import MovieList from '../MovieList/MovieList';
import img1 from '../../../../../../shared/img/1.png';
import img2 from '../../../../../../shared/img/2.png';
import img3 from '../../../../../../shared/img/3.png';
import img4 from '../../../../../../shared/img/4.png';
import img5 from '../../../../../../shared/img/5.png';
import img6 from '../../../../../../shared/img/6.png';
import img7 from '../../../../../../shared/img/7.png';
import img8 from '../../../../../../shared/img/8.png';
import img9 from '../../../../../../shared/img/9.png';
import img10 from '../../../../../../shared/img/10.png';
import img11 from '../../../../../../shared/img/11.png';
import img12 from '../../../../../../shared/img/12.png';
const categories = ['Trending','Top Rated', 'New Arrivals', 'Trailers','Coming Soon','Genre']
const movieProps = [
    {
        img:img1,
        title:'Fantastic Beasts...',
        genre:'Adventure, Family, Fantasy',
        score:'4.7'
    },
    {
        img:img2,
        title:'AssAssin’s Creed',
        genre:'Action, Adventure, Fantasy',
        score:'4.2'
    },
    {
        img:img3,
        title:'Now you see me 2',
        genre:'Action, Adventure, Comedy',
        score:'4.4'
    },
    {
        img:img4,
        title:'The Legend of Ta...',
        genre:'Action, Adventure, Drama',
        score:'4.3'
    },
    {
        img:img5,
        title:'Doctor Strange',
        genre:'Action, Adventure, Fantasy',
        score:'4.8'
    },
    {
        img:img6,
        title:'Captain America...',
        genre:'Action, Adventure, Sci-Fi',
        score:'4.9'
    },
    {
        img:img7,
        title:'Alice Through th...',
        genre:'Adventure, Family, Fantasy',
        score:'4.1'
    },
    {
        img:img8,
        title:'Finding Dory',
        genre:'Animation, Adventure, Comedy',
        score:'4.7'
    },
    {
        img:img9,
        title:'The BFG.',
        genre:'Adventure, Family, Fantasy',
        score:'3.2'
    },
    {
        img:img10,
        title:'Independence Day',
        genre:'Action, Sci-Fi',
        score:'3.9'
    },
    {
        img:img11,
        title:'Ice Age: Collisio...',
        genre:'Adventure, Comedy',
        score:'4.5'
    },
    {
        img:img12,
        title:'Moana',
        genre:'Action, Fantasy',
        score:'4.9'
    },
]
const Main = ({count}) =>{
    return(
        <section className='main'>
            <div className='container'>
                <MovieList count={count}  list={movieProps}/>
            </div>
        </section>
    )
}

export default Main