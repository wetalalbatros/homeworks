import React from 'react';
import styles from './CategoryList.css';

const CategoryList = ({text}) =>{
    const listItems= text.map(e=><li className='category-list-item'>{e}</li>);
    return(
        <div className='categories-block'>
            <div className='container'>
            <div className='categories'>
                <ul className='category-list'>
                    {listItems}
                </ul>
                <div className='templates'>
                    <span className='template active'><i className="fa fa-th-large"></i></span>
                    <span className='template'><i className="fa fa-th-list"></i></span>
                </div>
            </div>
            </div>
        </div>
        
        
    )
} 

export default CategoryList;