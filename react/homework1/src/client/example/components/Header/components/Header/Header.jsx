import React from 'react';
import Navbar from './../Navbar/Navbar'
import HeaderFooter from './../../HeaderFooter/HeaderFooter';
import styles from './Header.css';
const Header = () => {
    return(
    

        <header className='header'>
                    <Navbar/>
                    <div className='container'>
                    <div className='header-title-block'>
                        <h1 className='header-title'>the jungle book</h1>
                        <h3 className='header-subtitle'><pre>Adventure    Drama    Family    Fantasy   |   1h 46m</pre></h3>
                    </div>
                    <HeaderFooter/>
                    </div>
        </header>
      
    )
}

export default Header;
