import React from 'react';
import Logo from './../../../../../../shared/components/Logo/Logo'
import Button from './../../../../../../shared/components/Button/Button'

import styles from './Navbar.css';

const Navbar = () =>{
    return(
        <div className ='navbar'>
            <div className='container'>
                <div className='navbar-content'>
                    <Logo/>
                    <div className ='registration'>
                        <a className='search'><i className="fa fa-search"></i></a>
                        <Button className='btn transparent' text='Sign in'/>
                        <Button className='btn blue' text='Sign up'/>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Navbar;