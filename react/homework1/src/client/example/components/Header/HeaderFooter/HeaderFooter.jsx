import React from 'react';
import Score from './../../../../../shared/components/Score/Score';
import Button from './../../../../../shared/components/Button/Button'
import styles from './HeaderFooter.css';
const HeaderFooter = () =>{
    return(
        <div className='header-footer'>
            <div className='film-score'>
                <div className='stars'>
                    <span className='rating-star'><i className="fa fa-star"></i></span>
                    <span className='rating-star'><i className="fa fa-star"></i></span>
                    <span className='rating-star'><i className="fa fa-star"></i></span>
                    <span className='rating-star'><i className="fa fa-star"></i></span>
                    <span className='rating-star'><i className="fa fa-star"></i></span>
                </div>
                <Score score='4.8'/>
            </div>
            <div className='right-side'>
                <Button className='btn blue' text='Watch Now'/>
                <Button className='btn transparent' text='View Info'/>
                <Button className='btn transparent border' text='+ Favorites'/>
                <span className='more'><i className="fa fa-ellipsis-v"></i></span>
            </div>
        </div>
    )
}

export default HeaderFooter;