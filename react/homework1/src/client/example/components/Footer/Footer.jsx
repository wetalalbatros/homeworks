import React from 'react';
import styles from './Footer.css';
import Logo from './../../../../shared/components/Logo/Logo'
const Footer = () =>{
    return(
        <footer className='footer'>
            <div className='container'>
                <div className='footer-header'>
                     <div className='footer-contacts'>
                        <ul className='contact-list'>
                            <li className='contact-item'>About</li>
                            <li className='contact-item'>Terms of Service</li>
                            <li className='contact-item'>Contacts</li>
                        </ul>
                    </div>
                    <div className='footer-logo'>
                        <Logo/>
                    </div>
                    <div className='footer-links'>
                        <span className='footer-link'><i className="fa fa-facebook-f"></i></span>
                        <span className='footer-link'><i className="fa fa-twitter"></i></span>
                        <span className='footer-link'><i className="fa fa-pinterest-p"></i></span>
                        <span className='footer-link'><i className="fa fa-instagram"></i></span>
                        <span className='footer-link'><i className="fa fa-youtube-square"></i></span>
                    </div>
                </div>
            <div className='footer-copyright'>
                <p className='copyright-text'>Copyright © 2017 <span className='bold'>MOVIE</span>RISE. All Rights Reserved.</p>
            </div>
            </div>
        </footer>
    )
}

export default Footer;