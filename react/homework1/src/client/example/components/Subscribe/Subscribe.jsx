import React from 'react';
import styles from './Subscribe.css';
import Button from './../../../../shared/components/Button/Button'
const Subscribe =()=>{
    return(
    <div className='container'>
        
        <div className='subscribe'>
            <div className='wrap-con'></div>
            <div className='subscribe-content'>
                <div className='subscribe-title-block'>
                <h1 className='subscribe-title'>Receive information on the latest hit movies
                                            straight to your inbox.</h1>
            </div>
            
            <Button className='btn blue' text='Subscribe!'/>
            
            </div>
            
        </div>
            
    </div>
    )
}

export default Subscribe;