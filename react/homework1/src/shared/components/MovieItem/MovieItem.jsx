import React from 'react';
import Score from '../Score/Score';
import styles from './MovieItem.css'
const MovieItem = ({params}) =>{
    return(
        
        <div className='movie-item'>
            <div className='movie-img-block'>
                <img src={params.img} alt="" className='movie-img'/>
            </div>
            <div className='movie-info-block'>
                <h3 className ='movie-item-title'>{params.title}</h3>
                <Score score={params.score}/>
                <p className ='movie-item-genre'>{params.genre}</p>
                
            </div>
            
        </div>
    )
}

export default MovieItem