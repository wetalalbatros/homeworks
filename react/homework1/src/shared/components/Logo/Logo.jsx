import React from 'react';
import styles from './Logo.css'
const Logo = () =>{
    return(
        <a href="#" className='logo'><span className='bold'>MOVIE</span>RISE</a>
    )
}

export default Logo;