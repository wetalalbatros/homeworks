import React from 'react';
import style from './Score.css'
const Score = (props) =>{
    return(
        <div className='score-block'>
            <span className='score-value'>{props.score}</span>
        </div>
    )
}

export default Score;