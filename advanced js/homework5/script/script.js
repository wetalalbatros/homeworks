const titleList = document.querySelectorAll(".star-wars-title");
const episodeList = document.querySelectorAll(".star-wars-episode");
const crawlList = document.querySelectorAll(".star-wars-crawl");
const blockList = document.querySelectorAll(".star-wars");

const request = new XMLHttpRequest();

request.open('GET', "https://swapi.dev/api/films/");

request.responseType = 'json';

request.send();

let index=0;
request.onload = function() {
    if (request.status >= 300) { 
        console.log(`Ошибка ${request.status}: ${request.statusText}`); 
    } else { 
        blockList.forEach(e=>{
            e.firstChild.nextSibling.textContent = request.response.results[index].title;
            e.firstChild.nextSibling.nextSibling.nextSibling.textContent = request.response.results[index].episode_id;
            e.firstChild.nextSibling.nextSibling.nextSibling.nextSibling.nextSibling.textContent = request.response.results[index].opening_crawl;
            let list = e.querySelector(".characters-list");
            let characters = request.response.results[index].characters;
            let arr=[]
             characters.forEach(i=>{
                let newRequest = new XMLHttpRequest();
                newRequest.open('GET', i);
                newRequest.responseType = 'json';
                newRequest.send();
                newRequest.onload = function() {
                    
                    if (newRequest.status >= 300) { 
                        console.log(`Ошибка ${newRequest.status}: ${newRequest.statusText}`); 
                    } else { 
                         arr.push(newRequest.response.name)
                       if(characters.length === arr.length){
                        renderCharacters(arr);
                    }
                       
                     }
                    }
                    
            })
            function renderCharacters(arr){
                console.log(arr)
           let charactersList =  arr.map(elem => {
            return `<li>${elem}</li>`
           }).join("");
           
           list.innerHTML = charactersList;
        }
            index++
        })
    
        
    
};
}

// request.onerror = function() {
//     alert("Запрос не удался");
// };