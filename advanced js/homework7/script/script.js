const titleList = document.querySelectorAll(".star-wars-title");
const episodeList = document.querySelectorAll(".star-wars-episode");
const crawlList = document.querySelectorAll(".star-wars-crawl");
const blockList = document.querySelectorAll(".star-wars");
let index = 0;
const request = fetch("https://swapi.dev/api/films/");

request.then((result)=>{
    return result.json();
}).then((body)=>{
        blockList.forEach(e=>{
                        e.firstChild.nextSibling.textContent = body.results[index].title;
                        e.firstChild.nextSibling.nextSibling.nextSibling.textContent =body.results[index].episode_id;
                        e.firstChild.nextSibling.nextSibling.nextSibling.nextSibling.nextSibling.textContent = body.results[index].opening_crawl;
                        let list = e.querySelector(".characters-list");
                        let characters = body.results[index].characters;
                        let arr=[]
                      const qwq =   Promise.all(characters.map(item => fetch(item)))
                        qwq.then(responses => Promise.all(responses.map(response => response.json()))).
                        then(results => {
                           results.forEach(ewq=>{
                               arr.push(ewq.name)
                                    renderCharacters(arr)});
                           })
                      
                        function renderCharacters(arr){
                            let charactersList =  arr.map(elem => {
                                  return `<li>${elem}</li>`
                                     }).join("");
                                     list.innerHTML = charactersList;
                        }
                        index++
    })

})