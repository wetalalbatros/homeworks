

const  request = fetch('https://jsonplaceholder.typicode.com/users');   
const resolve = fetch('https://jsonplaceholder.typicode.com/posts');
const mainBlock = document.getElementById("list");
const addPost = document.getElementById("add-post");


request.then((result)=>{
    return result.json();
}).then((body)=>{
    resolve.then((result)=>{
        return result.json();
    }).then((data)=>{
        data.forEach((i)=>{
            body.forEach((elem)=>{
                if(i.userId === elem.id){
                    
                    const example = `<div class='post' data-id='${i.id}' data-user-id='${i.userId}'>
                                    <span class='delete-button' >x</span>
                                    <span class='user-name' contenteditable='true'>${elem.name}</span><span contenteditable='true' class='user-email'>${elem.email}</span>
                                    <p class='title' contenteditable='true'>${i.title}</p>
                                    <p class='user-text' contenteditable='true'>${i.body}</p>
                                        </div>`;
                    mainBlock.insertAdjacentHTML("beforeend",example);
                    const postsList = document.querySelectorAll(".post");
                    const userNameList = document.querySelectorAll(".user-name");
                    const userEmailList = document.querySelectorAll(".user-email");
                    const userTitleList = document.querySelectorAll(".title");
                    const userTextList = document.querySelectorAll(".user-text");
                    postsList.forEach((e)=>{
                        e.addEventListener("click",changePost)
                    })
                    // userNameList.forEach((e)=>{
                    //     e.addEventListener("blur",changeText)
                    // })
                    // userEmailList.forEach((e)=>{
                    //     e.addEventListener("blur",changeText)
                    // })
                    // userTitleList.forEach((e)=>{
                    //     e.addEventListener("blur",changeText)
                    // })
                    // userTextList.forEach((e)=>{
                    //     e.addEventListener("blur",changeText)
                    // })
                }
            })
        })
    })
})




function changePost(e){
     const dataId = this.getAttribute("data-id");
    if(e.target.classList.contains("delete-button")){
        fetch(`https://jsonplaceholder.typicode.com/posts/${dataId}`, {
        method: 'DELETE'
        })
       this.style.display="none";
      
        
    }
    if(e.target.className.includes("title")||e.target.className.includes("user-text")||e.target.className.includes("user-name")||e.target.className.includes("user-email")){
        e.target.addEventListener("blur",changeText)
    }
}

function changeText(){
   
    const dataId = this.closest(".post").getAttribute("data-id");
    const dataUserId = this.closest(".post").getAttribute("data-user-id");
    const title = this.closest(".post").querySelector(".title").textContent;
    const body = this.closest(".post").querySelector(".user-text").textContent;
    fetch(`https://jsonplaceholder.typicode.com/posts/${dataId}`, {
        method: 'PUT',
        body: JSON.stringify({
          id: dataId,
          title: title,
          body: body,
          userId: dataUserId
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8"
        }
      })

}

addPost.addEventListener("click",function(){
    const modal = document.getElementById("modal");
    const blocker = document.getElementById("blocker");
    modal.classList.add("active");
    blocker.classList.add("active");

})

const closeModal =  document.getElementById("close-modal");
closeModal.addEventListener("click",function(){
    const parent = this.closest(".modal");
    const blocker =  document.getElementById("blocker");
    parent.classList.remove("active");
    blocker.classList.remove("active")
})
let index = 100;
const createPost = document.getElementById("create-post");
createPost.addEventListener("click",function(){
    const inputTitle = document.getElementById("new-title").value;
    const inputText = document.getElementById("new-text").value;
    if(inputText&&inputTitle){ 
        fetch('https://jsonplaceholder.typicode.com/posts', {
                    method: 'POST',
                    body: JSON.stringify({
                    title: inputTitle,
                    body: inputText,
                    userId: 1
                    }),
                    headers: {
                    "Content-type": "application/json; charset=UTF-8"
                    }
                })
        const example = `<div class='post' data-id='${index++}' data-user-id='1'>
                                    <span class='delete-button' >x</span>
                                    <span class='user-name' contenteditable='true'>Leanne Graham</span><span contenteditable='true' class='user-email'>Sincere@april.biz</span>
                                    <p class='title' contenteditable='true'>${inputTitle}</p>
                                    <p class='user-text' contenteditable='true'>${inputText}</p>
                                        </div>`;
                    mainBlock.insertAdjacentHTML("afterbegin",example);
                   
    }
    const modal = document.getElementById("modal");
    const blocker =  document.getElementById("blocker");
    modal.classList.remove("active");
    blocker.classList.remove("active")
})