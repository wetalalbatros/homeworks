class HamburgerException  {
    constructor(message){ 
    this.message = message;
    this.name = 'HamburgerException';
    }   
}

class Hamburger {
    constructor(size,stuffing){
        if(!size){
            throw new HamburgerException("no size given");
         } else if (!stuffing){
            throw new HamburgerException("no stuffing given");
         } else{
          this.size= size;
          this.stuffing=stuffing;
          this.topping=[];
         }
    }
    addTopping(topping){
        
        if(this.topping.includes(topping)){
            throw new HamburgerException("этот топпинг уже добавлен")
         }else {  
            this.topping.push(topping);
         }
    }

    removeTopping(topping){
    var index = this.topping.indexOf(topping);
    if(index == "-1"){
       throw new HamburgerException("такого топпинга нет в списке добавленых")
    }
    this.topping.splice(index,1);
    }

    getToppings(){
        if (this.topping.length == null){
            throw new HamburgerException("В вашем гамбургере нету топпингов!") ;
         } else {
           return this.topping;
         }
    }

    getSize(){
        return this.size.size;
    }

    getStuffing(){
        return this.stuffing.stuffing;
    }

    calculatePrice(){
        var sum = 0;
        sum += this.size.price+ this.stuffing.price;
        this.topping.forEach((e)=>{
        sum += e.price;
        })
         return "стоимость гамбургера = " + sum +" грн";
    }

    calculateKkal(){
        var sum = 0;
        sum += this.size.kkal +this.stuffing.kkal;
        this.topping.forEach((e)=>{
            sum+=e.kkal;
        })
        return "сумма калорий " +sum + " kkal";
    }
  }
    Hamburger.SIZE_SMALL = {
        size:"small",
        price:50,
        kkal:20
     };
     Hamburger.SIZE_LARGE = {
        size:"large",
        price:100,
        kkal:40
     };
     Hamburger.STUFFING_CHEESE = {
        stuffing:"cheese",
        price:10,
        kkal:20
     };
     Hamburger.STUFFING_SALAD = {
        stuffing:"salad",
        price:20,
        kkal:5
     };
     Hamburger.STUFFING_POTATO = {
        stuffing:"potato",
        price:15,
        kkal:10
     };
     Hamburger.TOPPING_MAYO = {
        topping:"mayonese",
        price:20,
        kkal:5
     };
     Hamburger.TOPPING_SPICE = {
        topping:"spicy",
        price:15,
        kkal:0
     };
    
  try{
   hamburger = new Hamburger( Hamburger.SIZE_LARGE,Hamburger.STUFFING_CHEESE);
 }catch(e){
    console.error(e.name +" "+ e.message); 
 }

 try{
    hamburger.addTopping(Hamburger.TOPPING_SPICE);
 }catch(e){
    console.error(e.name +" "+ e.message); 
 }

 try{
    hamburger.removeTopping(Hamburger.TOPPING_MAYO);
 }catch(e){
    console.error(e.name +" "+ e.message); 
 }

 try{
     console.log(hamburger.getToppings());
 }catch(e){
    console.error(e.name +": "+ e.message); 
 }

 console.log(hamburger)
