


    function createNewUser() {
        let userName = prompt(`Введите имя`);
        let userLastName = prompt(`Введите фамилию`);
        let birthday = prompt(`Введите дату рождения в формате дд.мм.гггг`);
        const newUser = {
            firstName:userName,
            lastName:userLastName,
            birthDay:birthday,
            getLogin(){
                let shortName = this.firstName[0]+this.lastName;
                return (shortName.toLocaleLowerCase());
            },
            getAge() {
                const nowDate = new Date();
                let birthday = this.birthDay.split(".");
                let birthdayYear = +birthday[2];
                let birthdayMonth = +birthday[1];
                let birthdayDay = +birthday[0];
                let age = nowDate.getFullYear() - birthdayYear;
                if (birthdayMonth > nowDate.getMonth() || (birthdayMonth === nowDate.getMonth() && birthdayDay > nowDate.getDate())) {
                    age--;
                }
                return age;
            },
            getPassword(){
                let password = this.firstName[0].toUpperCase()+this.lastName.toLowerCase()+this.birthDay.slice(6);
                return (password);
            },
        };
        return newUser;
    }
    const user1 = createNewUser();
    console.log(user1);
    console.log(user1.getLogin());
    console.log(user1.getAge());
    console.log(user1.getPassword());




